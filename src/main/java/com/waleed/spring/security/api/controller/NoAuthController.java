package com.waleed.spring.security.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/noAuth/rest")
public class NoAuthController {
	
	//Used Basic Auth

	@GetMapping("/sayHi")
	public String sayHi() {
		return "Hi!!! Login Successful";
	}

}
